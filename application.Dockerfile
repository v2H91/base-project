# Start with a base image containing Java runtime
FROM openjdk:11.0.11-jre-slim
# Add a volume pointing to /tmp
VOLUME /tmp
# Add argument spring profile
#ARG ENV_PROFILE
ENV TZ="Asia/Ho_Chi_Minh"
# Add the application's jar to the container
ADD target/master-data-application-1.0.0.jar master-data-worker-1.0.0.jar
# Run the jar file
ENTRYPOINT exec java -jar master-data-worker-0.0.1.jar
EXPOSE 8080