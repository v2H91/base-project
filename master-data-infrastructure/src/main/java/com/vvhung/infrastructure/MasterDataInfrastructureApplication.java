package com.vvhung.infrastructure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterDataInfrastructureApplication {

    public static void main(String[] args) {
        SpringApplication.run(MasterDataInfrastructureApplication.class, args);
    }

}
