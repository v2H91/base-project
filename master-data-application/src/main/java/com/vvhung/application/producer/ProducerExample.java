package com.vvhung.application.producer;

import com.vvhung.business.domain.event.ItemEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
public class ProducerExample {
    @Autowired
    private KafkaTemplate<String, ItemEvent> kafkaTemplate;

    private final static String TOPIC_NAME = "test";

    public void sendMessage(ItemEvent msg) {
        ListenableFuture<SendResult<String, ItemEvent>> future = kafkaTemplate.send(TOPIC_NAME, msg);
        future.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                System.out.println(ex.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, ItemEvent> result) {
                System.out.println(result.getRecordMetadata().offset());
            }
        });
    }


}
