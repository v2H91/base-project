package com.vvhung.application.controller;


import com.vvhung.application.producer.ProducerExample;
import com.vvhung.business.domain.event.ItemEvent;
import com.vvhung.common.infrastructures.rest.BaseResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/test/v1/test")
public class TestApiController {
    @Autowired
    ProducerExample producerExample;

    @ApiOperation(value = "t̀est API get")
    @PostMapping(path = "/test")
    public BaseResponse<String> getProjectById(@RequestBody ItemEvent message) {
        producerExample.sendMessage(message);
        return BaseResponse.ofSucceeded("test API success");
    }
}
