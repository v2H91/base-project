package com.vvhung.application.controller;

import com.vvhung.application.usecase.ImportUseCase;
import com.vvhung.business.domain.model.JobLog;
import com.vvhung.common.infrastructures.rest.BaseResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(DataController.JOBS_V1)
public class DataController {

    public static final String JOBS_V1 = "/v1/data";

    private final ImportUseCase importUseCase;

    @PostMapping(value = "/import", consumes = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public BaseResponse<JobLog> createJob(final HttpServletRequest request) throws IOException {
        ServletInputStream inputStream = request.getInputStream();
        JobLog jobLog = importUseCase.importData(inputStream);
        return BaseResponse.ofSucceededWithRequestId(jobLog, jobLog.getRequestId());
    }
}
