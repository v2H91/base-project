package com.vvhung.application.configs;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic CreateTopic() {
        return TopicBuilder.name("test").partitions(2).replicas(1).build();
    }
}
