package com.vvhung.application.utils.excel;

import java.lang.reflect.Field;
import java.util.List;

public class ExcelMapper {
    public static void transferData(Object targetObject, String[] row, String[] fieldsName) {
        for (int cellIndex = 0; cellIndex < row.length; cellIndex++) {
            setField(targetObject, standardFieldName(fieldsName[cellIndex]), row[cellIndex]);
        }
    }

    public static void transferData(Object targetList, String[][] rows, String[] fieldsName, Object instanceOfList) {
        for (String[] row : rows) {
            Object targetObject = instanceOfList.getClass().getDeclaredConstructors();

            transferData(targetObject, row, fieldsName);

            ((List<Object>) targetList).add(targetObject);
        }
    }

    private static String standardFieldName(String fieldName) {
        if (fieldName == null || fieldName.replace(" ", "").equals("")) return "";

        StringBuilder result = new StringBuilder();
        String[] splitString = fieldName.replace(" ", "").toLowerCase().split("_");

        for (String target : splitString) {
            if (!target.equals("")) result.append(Character.toUpperCase(target.charAt(0))).append(target.substring(1));
        }

        return Character.toLowerCase(result.charAt(0)) + result.substring(1);
    }

    private static boolean setField(Object targetObject, String fieldName, Object fieldValue) {
        Field field;
        try {
            field = targetObject.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            field = null;
        }
        Class superClass = targetObject.getClass().getSuperclass();
        while (field == null && superClass != null) {
            try {
                field = superClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                superClass = superClass.getSuperclass();
            }
        }
        if (field == null) {
            return false;
        }
        field.setAccessible(true);
        try {
            field.set(targetObject, fieldValue);
            return true;
        } catch (IllegalAccessException e) {
            return false;
        }
    }
}
