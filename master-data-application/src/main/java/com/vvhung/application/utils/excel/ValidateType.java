package com.vvhung.application.utils.excel;

public enum ValidateType {
    Require("Require"),
    Date("Date"),
    Number("Number"),
    Boolean("Boolean"),
    Duplicate("Duplicate");

    private String value;

    ValidateType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
