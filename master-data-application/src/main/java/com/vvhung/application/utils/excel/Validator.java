package com.vvhung.application.utils.excel;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Validator {
    static final String DATE_FORMAT = "yyyy-MM-dd";

    public static List<String>[] validate(String[] row, String[] configs) {
        List<String>[] errors = new ArrayList[configs.length];

        for (int index = 0; index < configs.length; index++) {
            if (row.length > index)
                errors[index] = validate(row[index], configs[index]);
        }

        return errors;
    }

    public static List<String>[] validateColumn(String[] column, String config) {
        List<String>[] errors = new ArrayList[column.length];

        if (config.contains(ValidateType.Duplicate.getValue())) errors = validateDuplicate(column);

        return errors;
    }

    private static List<String>[] validateDuplicate(String[] column) {
        List<String>[] errors = new ArrayList[column.length];

        for (int i = 0; i < column.length; i++) {
            errors[i] = new ArrayList<>();
            for (int j = 0; j < column.length; j++) {
                if (i != j && column[i] != null && column[i].equals(column[j])) {
                    errors[i].add(ValidateError.Duplicate.getValue());
                    break;
                }
            }
        }

        return errors;
    }

    private static List<String> validate(String data, String config) {
        List<String> errors = new ArrayList<>();

        if (config.contains(ValidateType.Require.getValue()) && !validRequire(data))
            errors.add(ValidateError.Require.getValue());
        if (config.contains(ValidateType.Date.getValue()) && data != null && !validDate(data))
            errors.add(ValidateError.Date.getValue());
        if (config.contains(ValidateType.Number.getValue()) && data != null && !validNumber(data))
            errors.add(ValidateError.Number.getValue());
        if (config.contains(ValidateType.Boolean.getValue()) && data != null && !validBoolean(data))
            errors.add(ValidateError.Boolean.getValue());

        return errors;
    }

    private static boolean validRequire(String data) {
        return data != null && !data.isEmpty() && !data.trim().isEmpty();
    }

    private static boolean validDate(String data) {
        try {
            LocalDate.parse(data);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean validNumber(String data) {
        if (data == null) {
            return false;
        }
        try {
            Double.parseDouble(data);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean validBoolean(String data) {
        data = data.toLowerCase().trim();
        return "true".equals(data) || "false".equals(data) || "yes".equals(data) || "no".equals(data);
    }
}
