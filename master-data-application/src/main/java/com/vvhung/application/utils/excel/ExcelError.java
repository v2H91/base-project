package com.vvhung.application.utils.excel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExcelError {
    private int rowIndex;
    private int columnIndex;
    private String errorMessage;
    private String errorMessageDetail;
    private String fieldName;
}
