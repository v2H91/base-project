package com.vvhung.application.utils.excel;


import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class StreamObjectUpload {


    public String streamObjectUpload(String fileName, ByteArrayOutputStream bos) throws IOException {

        if ("cloud property".equals("local/import_files/errors")) {
            //todo up error file to cloud
            return "url";
        } else {
            FileOutputStream out = FileUtils.openOutputStream(new File("temp/" + fileName));
            bos.writeTo(out);
            out.close();
            return "temp/" + fileName;
        }
    }

}