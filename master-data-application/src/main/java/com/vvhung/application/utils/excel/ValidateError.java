package com.vvhung.application.utils.excel;

public enum ValidateError {
    Require("is required"),
    Date("is incorrect date format or is not exist date"),
    Number("is not a number"),
    Boolean("is not a boolean"),
    Duplicate("is duplicated");

    private String value;

    ValidateError(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
