package com.vvhung.application.utils.excel;

import com.vvhung.common.shared.constant.ImportError;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaType;
import org.apache.poi.ss.formula.SharedFormula;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.usermodel.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class SheetData {
    private Row row;
    private final String[][] data;
    private Row sharedFormulas;
    private List<CellStyle> sharedStyles;
    private List<CellStyle> sharedErrorStyles;
    private String[] fieldsName;
    private String[] configsGeneral = new String[0];
    private List<ExcelError> errors;
    private int validateRowIndex = -1;
    private int validateColumnIndex = 0;
    private final int dataBeginIndex;
    private int fieldsNameIndex;
    //    private int[] totalErrorInRow;
    private int rowIndex = 0;
    private int errorRowIndex = 0;
    private int cellCount;
    private boolean emptyData;
    private int totalEmptyData = 0;
    private int totalError = 0;

    public SheetData(Sheet sheet, int headerIndex, int dataBeginIndex) {
        this.dataBeginIndex = dataBeginIndex - 1;

        this.data = new String[1][];
        this.errors = new ArrayList<>();
    }

    public void setValidateIndies(int validateRowIndex, int validateColumnIndex) {
        this.validateRowIndex = validateRowIndex;
        this.validateColumnIndex = validateColumnIndex;
    }

    public void loadData(Row row) {
        this.row = row;
        this.errors = new ArrayList<>();

        if (rowIndex == 0) {
            cellCount = row.getLastCellNum() + 1;
            while (cellCount > 0 && (row.getCell(cellCount - 1) == null || row.getCell(cellCount - 1).getCellType() == CellType.BLANK))
                cellCount--;
        }

        String[] rowData = new String[cellCount];

        for (int cellIndex = 0; cellIndex < cellCount; cellIndex++) {
            String cellValue = formatCellValue(row.getCell(cellIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));

            rowData[cellIndex] = cellValue.trim().equals("") ? null : cellValue.trim();
        }
        if (rowIndex >= dataBeginIndex) {
            data[0] = rowData;
            emptyData = checkEmpty(rowData);
            totalEmptyData += emptyData ? 1 : 0;
            if (rowIndex == dataBeginIndex) sharedFormulas = row;
        }
        if (rowIndex == fieldsNameIndex - 1) fieldsName = rowData;
        if (rowIndex == validateRowIndex - 1) configsGeneral = standardConfig(rowData);
        rowIndex++;
    }

    private String[] standardConfig(String[] config) {
        for (int i = 0; i < cellCount; i++) {
            config[i] = i >= validateColumnIndex && config[i] != null ? config[i].toUpperCase() : "";
            config[i] = "Y".equals(config[i]) || "YES".equals(config[i]) || "Required".equals(config[i]) ?
                    ValidateType.Require.getValue() : "";
        }

        return config;
    }

    public void validate(HashMap<String, String[]> validateActionConfigs, String[] validateConfigs, int actionIndex) {
        if (rowIndex >= dataBeginIndex) {
            String[] row = data[0];
            String action = row[actionIndex - 1];
            String[] configsByAction = validateActionConfigs.get(action);
            String[] configs = new String[configsByAction != null ? configsByAction.length : 0];

            data[0][actionIndex - 1] = action;
            if (configsByAction != null) {
                for (int i = 0; i < configsByAction.length; i++) {
                    if (configsGeneral.length == 0) configs[i] = configsByAction[i];
                    else {
                        String configGeneral = i < configsGeneral.length ? configsGeneral[i] : "";

                        configs[i] = configsByAction[i] + configGeneral;
                        configs[i] = i < validateColumnIndex || ValidateType.Require.getValue().equals(configGeneral)
                                ? configs[i] : configs[i].replace(ValidateType.Require.getValue(), "");
                    }
                }
                addRowErrors(Validator.validate(row, configs), rowIndex);
            }
            else
                errors.add(new ExcelError(rowIndex, actionIndex - 1, ImportError.EI0006.getValue(), "", ""));
        }
        totalError += errors.size();
    }

    private void addRowErrors(List<String>[] rowErrors, int rowIndex) {
        for (int columnIndex = 0; columnIndex < rowErrors.length; columnIndex++) {
            if (rowErrors[columnIndex] != null && rowErrors[columnIndex].size() > 0)
                for (String errorMessage : rowErrors[columnIndex]) {
                    errors.add(new ExcelError(rowIndex, columnIndex, errorMessage, "", getFiledName(columnIndex)));
                }
        }
    }

    public void addError(String errorMessage, String errorMessageDetail, String fieldNames) {
        for (String fieldName : fieldNames.split(","))
            errors.add(new ExcelError(rowIndex - 1, getColumnIndex(fieldName), errorMessage, errorMessageDetail, fieldName));
        totalError++;
    }

    private void copyRow(Row newRow, Row sourceRow, boolean useSharedStyle) {
        for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
            Cell oldCell = sourceRow.getCell(i);
            Cell newCell = newRow.createCell(i);

            if (oldCell == null) {
                continue;
            }

            if (!useSharedStyle) {
                CellStyle newCellStyle = newRow.getSheet().getWorkbook().createCellStyle();

                newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
                newCell.setCellStyle(newCellStyle);
            } else newCell.setCellStyle(sharedStyles.get(i >= cellCount ? cellCount - 1 : i));
            if (oldCell.getCellType() != CellType.FORMULA)
                newCell.setCellType(oldCell.getCellType());
            setCellValue(oldCell, newCell);
        }
    }

    private void setCellValue(Cell oldCell, Cell newCell) {
        switch (oldCell.getCellType()) {
            case BLANK:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            case ERROR:
                newCell.setCellValue(oldCell.getStringCellValue().replace("ERROR:  ", ""));
                break;
            case BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case FORMULA:
                newCell.setCellFormula(getCellFormula(oldCell, newCell));
                break;
            case _NONE:
                break;
            case NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case STRING:
                newCell.setCellValue(oldCell.getRichStringCellValue());
                break;
        }
    }

    private String getCellFormula(Cell cell, Cell newCell) {
        try {
            String formula = !StringUtils.isEmpty(cell.getCellFormula()) ? cell.getCellFormula() : sharedFormulas.getCell(cell.getColumnIndex()).getCellFormula();
            int formulaRow = !StringUtils.isEmpty(cell.getCellFormula()) ? newCell.getRowIndex() - cell.getRowIndex() : newCell.getRowIndex() - dataBeginIndex;
            SharedFormula sharedFormula = new SharedFormula(SpreadsheetVersion.EXCEL2007);
            Ptg[] sharedFormulaPtg = FormulaParser.parse(formula, null, FormulaType.CELL, 0);
            Ptg[] convertedFormulaPtg = sharedFormula.convertSharedFormulas(sharedFormulaPtg, formulaRow, 0);

            return FormulaRenderer.toFormulaString(null, convertedFormulaPtg);
        } catch (Exception ex) {
            log.error(ex.toString());
            return cell.getStringCellValue();
        }
    }

    public void saveRowToExcel(Sheet errorSheet) {
        saveRowToExcel(errorSheet, false);
    }

    public void saveRowToExcel(Sheet errorSheet, boolean useSharedStyle) {
        Row errorRow = errorSheet.createRow(errorRowIndex);

        copyRow(errorRow, row, useSharedStyle);
        errorRowIndex++;
    }

    public void saveErrorToExcel(Sheet errorSheet) {
        if (errors == null || errors.size() == 0) return;

        if (errorRowIndex == dataBeginIndex) {
            saveRowToExcel(errorSheet);

            sharedStyles = new ArrayList<>();
            sharedErrorStyles = new ArrayList<>();
            for (int cellIndex = 0; cellIndex < cellCount; cellIndex++) {
                Cell cell = errorSheet.getRow(errorRowIndex - 1).getCell(cellIndex);
                CellStyle cellStyle = cell != null ? cell.getCellStyle() : errorSheet.getWorkbook().createCellStyle();
                CellStyle errorCellStyle = errorSheet.getWorkbook().createCellStyle();

                cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
                errorCellStyle.cloneStyleFrom(cellStyle);
                sharedStyles.add(cellStyle);
                sharedErrorStyles.add(errorCellStyle);
            }
        } else saveRowToExcel(errorSheet, true);
        for (ExcelError error : errors) {
            Row errorRow = errorSheet.getRow(errorRowIndex - 1);
            Cell errorCell = errorRow.getCell(error.getColumnIndex());
            String commentText = error.getErrorMessageDetail().isEmpty() ? error.getErrorMessage() :
                    String.format("%s(%s)", error.getErrorMessage(), error.getErrorMessageDetail());

            errorCell = errorCell == null ? errorRow.createCell(error.getColumnIndex()) : errorCell;
            changeCellBackgroundColor(errorCell, IndexedColors.YELLOW.getIndex());
            addComment(errorCell, commentText);
        }
    }

    private void changeCellBackgroundColor(Cell cell, short backgroundColorIndex) {
        if (cell == null) return;

        CellStyle cellStyle = sharedErrorStyles.get(cell.getColumnIndex());

        cellStyle.setFillForegroundColor(backgroundColorIndex);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cell.setCellStyle(cellStyle);
    }

    private void addComment(Cell cell, String commentText) {
        if (cell == null) return;

        CreationHelper factory = cell.getSheet().getWorkbook().getCreationHelper();
        Comment comment = cell.getCellComment();

        if (comment == null) {
            ClientAnchor anchor = factory.createClientAnchor();

            anchor.setCol1(cell.getColumnIndex());
            anchor.setCol2(cell.getColumnIndex() + 4);
            anchor.setRow1(cell.getRowIndex());
            anchor.setRow2(cell.getRowIndex() + 8);

            Drawing<?> drawing = cell.getSheet().createDrawingPatriarch();

            comment = drawing.createCellComment(anchor);
            comment.setString(factory.createRichTextString(commentText));
            comment.setAuthor("author");
        } else {
            comment.setString(factory.createRichTextString(comment.getString() + ";" + commentText));
        }

        cell.setCellComment(comment);
    }

    public int getRowIndex() {
        return rowIndex;
    }

    private int getColumnIndex(String fieldName) {
        if (fieldName == null) return 0;

        for (int index = 0; index < fieldsName.length; index++)
            if (fieldName.equals(fieldsName[index]) || (fieldsName[index] == null && fieldName.isEmpty()))
                return index;

        return 0;
    }

    private String getFiledName(int columnIndex) {
        return fieldsName != null && fieldsName.length > columnIndex ? fieldsName[columnIndex] + "" : "";
    }

    public int getTotalEmptyData() {
        return totalEmptyData;
    }

    public boolean isNotEmptyData() {
        return !emptyData;
    }

    private boolean checkEmpty(String[] rowData) {
        for (String cell : rowData)
            if (cell != null) return false;

        return true;
    }

    private String toString(double d) {
        if (d == Math.floor(d)) {
            return String.format("%.0f", d);
        } else {
            return Double.toString(d);
        }
    }

    private String formatCellValue(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        DateFormat dateFormat = new SimpleDateFormat(Validator.DATE_FORMAT);

        if (rowIndex >= dataBeginIndex &&  (cell.getCellType() == CellType.ERROR || "#N/A".equals(cell.getStringCellValue())))
            errors.add(new ExcelError(cell.getRowIndex(), cell.getColumnIndex(), ImportError.EI0012.getValue(),
                    "", fieldsName[cell.getColumnIndex()]));
        if (cell.getCellType() == CellType.NUMERIC) {
            if (DateUtil.isCellDateFormatted(cell)) {
                return dateFormat.format(cell.getDateCellValue());
            } else {
                return toString(cell.getNumericCellValue());
            }
        }
        if (cell.getCellType() == CellType.FORMULA) {
            switch (cell.getCachedFormulaResultType()) {
                case BOOLEAN:
                    return String.valueOf(cell.getBooleanCellValue());
                case NUMERIC:
                    return toString(cell.getNumericCellValue());
            }
        }
        if (cell instanceof com.monitorjbl.xlsx.impl.StreamingCell) {
            return cell.getStringCellValue();
        }

        return formatter.formatCellValue(cell);
    }

    public void transferData(Object targetList, Object instanceOfList) throws InstantiationException, IllegalAccessException {
        if (rowIndex >= dataBeginIndex)
            ExcelMapper.transferData(targetList, data, fieldsName, instanceOfList);
    }

    public String[] getColumn(int columnIndex) {
        String[] column = new String[1];

        column[0] = data[0][columnIndex - 1];

        return column;
    }

    public int getTotalError(int rowIndex) {
        return errors.size();
    }

    public int getTotalError() {
        return totalError;
    }

    public List<ExcelError> getErrors() {
        return errors;
    }

    public void setFieldsName(int fieldsNameIndex) {
        this.fieldsNameIndex = fieldsNameIndex;
    }

    public void setFieldsName(String[] fieldsName) {
        this.fieldsName = fieldsName;
    }
}
