package com.vvhung.application.usecase;

import com.vvhung.common.shared.utils.ErrorField;
import com.vvhung.common.shared.utils.ValidationUtil;


import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractReadExcelFile {
    protected <T> void validate(T object) throws Exception {
        List<ErrorField> errorFields = ValidationUtil.validate(object);
        if (!errorFields.isEmpty()) {
            String errors = errorFields.stream()
                    .map(errorField -> "field: " + errorField.getField() + ", message: " + errorField.getMessage())
                    .collect(Collectors.joining(";"));
            throw new Exception(errors);
        }
    }
}
