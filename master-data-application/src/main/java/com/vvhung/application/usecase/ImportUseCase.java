package com.vvhung.application.usecase;


import com.vvhung.business.domain.model.JobLog;

import java.io.InputStream;

public interface ImportUseCase {
    JobLog importData(InputStream is);

}
