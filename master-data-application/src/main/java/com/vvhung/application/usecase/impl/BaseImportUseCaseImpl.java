package com.vvhung.application.usecase.impl;

import com.vvhung.application.utils.excel.ExcelError;
import com.vvhung.application.utils.excel.SheetData;
import com.vvhung.business.business.usecases.JobErrorUseCase;
import com.vvhung.business.domain.model.JobError;
import com.vvhung.business.domain.model.JobLog;
import com.vvhung.common.shared.constant.ImportAction;
import com.vvhung.common.shared.constant.ImportError;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.*;

@Slf4j
public abstract class BaseImportUseCaseImpl<T, U> {
    protected final int HEADER_INDEX = 1;
    protected int VALIDATE_ROW_INDEX = 0;
    protected int VALIDATE_COLUMN_INDEX = 0;
    protected final int FILED_NAME_INDEX = 3;
    protected final int BEGIN_DATA_INDEX = 4;
    protected final int ACTION_INDEX = 2;
    private HashMap<String, String[]> actionConfigs;
    private T instantOfGeneric;

    protected JobLog jobLog;
    protected Sheet sheet;
    protected SheetData sheetData;

    protected abstract void processData(List<T> listData);
    protected abstract JobErrorUseCase getJobErrorUseCase();
    protected abstract Validator getValidator();

    protected void init(HashMap<String, String[]> actionConfigs, T instantOfGeneric) {
        this.actionConfigs = actionConfigs;
        this.instantOfGeneric = instantOfGeneric;
    }

    protected boolean importExcel(Sheet sheet, Sheet errorSheet, JobLog jobLog) throws InstantiationException, IllegalAccessException {
        this.jobLog = jobLog;
        this.sheet = sheet;

        sheetData = new SheetData(sheet, HEADER_INDEX, BEGIN_DATA_INDEX);

        String[] configs = new String[]{"", "", "", "", "", "", ""};

        log.info(String.format("Start import %s!!!", instantOfGeneric.getClass().getSimpleName()));
        sheetData.setValidateIndies(VALIDATE_ROW_INDEX, VALIDATE_COLUMN_INDEX);
        sheetData.setFieldsName(FILED_NAME_INDEX);
        for (Row row : sheet) {
            List<T> listData = new ArrayList<>();

            sheetData.loadData(row);
            if (sheetData.getRowIndex() < BEGIN_DATA_INDEX) sheetData.saveRowToExcel(errorSheet);
            if (sheetData.getRowIndex() >= BEGIN_DATA_INDEX && sheetData.isNotEmptyData()) {
                sheetData.validate(actionConfigs, configs, ACTION_INDEX);
                sheetData.transferData(listData, instantOfGeneric);

                saveValidateErrors();
                if (extendValidate(listData.get(0))) {
                    processData(listData);
                }
                sheetData.saveErrorToExcel(errorSheet);
            }
            if (sheetData.getRowIndex() % 100 == 0)
                log.info(String.format("%s sheet - Row processed: %s", instantOfGeneric.getClass().getSimpleName(), sheetData.getRowIndex()));
            if (sheetData.getTotalEmptyData() > 10)
                break;
        }
        log.info(String.format("Finish import %s!!!", instantOfGeneric.getClass().getSimpleName()));
        return sheetData.getTotalError() > 0;
    }

    private boolean extendValidate(T data) {
        if (data == null) return true;
        if (sheetData.getErrors().size() == 0 || !sheetData.getErrors().get(0).getErrorMessage().equals(ImportError.EI0006.getValue())) {
            String[] actions = sheetData.getColumn(ACTION_INDEX);
            ImportAction action = StringUtils.hasText(actions[0]) ? ImportAction.valueOf(actions[0]) : ImportAction.INSERT;
            HashMap<String, List<String>> regexes = new HashMap<>() {{
                put("(.*) must be greater than or equal to ([^ ]*)", Arrays.asList("EI0010", "$1,$2"));
                put("(.*) must be greater than ([^ ]*)", Arrays.asList("EI0010", "$1,$2"));
                put("(.*) must be in (.*) to (.*) range", Arrays.asList("EI0010", "$1"));
                put("(.*) must be positive number", Arrays.asList("EI0010", "$1"));
                put("(.*) must be Yes or No", Arrays.asList("EI0004", "$1"));
                put("(.*) must be in list: (.*)", Arrays.asList("EI0010", "$1"));
            }};

            if (action == ImportAction.UPDATE || action == ImportAction.INSERT) {
                regexes.put("one of (.*) or (.*) is required", Arrays.asList("EI0009", "$1,$2"));
            }

            List<ConstraintViolation<T>> errors = new ArrayList<>(getValidator().validate(data));
            int errorCount = 0;

            errors.sort(Comparator.comparing(ConstraintViolation<T>::getMessage));

            for (ConstraintViolation<T> error : errors) {
                for (Map.Entry<String, List<String>> entry : regexes.entrySet()) {
                    String regex = entry.getKey();
                    List<String> config = entry.getValue();

                    if (error.getMessage().matches(regex)) {
                        getJobErrorUseCase().saveJobError(createJobError(error.getMessage().replaceAll(regex, config.get(1)), ImportError.valueOf(config.get(0)), error.getMessage()));
                        errorCount++;
                        break;
                    }
                }
            }

            return errorCount == 0;
        }
        return true;
    }

    protected void saveValidateErrors() {
        for (ExcelError error : sheetData.getErrors()) {
            getJobErrorUseCase().saveJobError(createJobError(error.getFieldName(), ImportError.fromString(error.getErrorMessage()), "", false));
        }
    }

    protected JobError createJobError(String fieldName, ImportError importError) {
        return createJobError(fieldName, importError, "", true);
    }

    protected JobError createJobError(String fieldName, ImportError importError, String errorMessageDetail) {
        return createJobError(fieldName, importError, errorMessageDetail, true);
    }

    protected JobError createJobError(String fieldName, ImportError importError, String errorMessageDetail, boolean addError) {
        if (addError) {
            sheetData.addError(importError.getValue(), errorMessageDetail, fieldName);
        }

        return JobError.builder()
                .requestId(jobLog.getRequestId())
                .errorCode(importError.name())
                .errorMessage(importError.getValue())
                .errorMessageDetail(errorMessageDetail)
                .column(fieldName)
                .row(sheetData.getRowIndex())
                .sheetName(sheet.getSheetName())
                .build();
    }
}
