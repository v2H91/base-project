package com.vvhung.application.usecase;

import com.vvhung.business.domain.model.JobLog;
import org.apache.poi.ss.usermodel.Sheet;

public interface TemplateUseCase {
    boolean importExcel(Sheet sheet, Sheet errorSheet, JobLog jobLog) throws InstantiationException, IllegalAccessException;
}
