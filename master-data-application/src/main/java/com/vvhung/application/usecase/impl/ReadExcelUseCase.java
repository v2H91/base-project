package com.vvhung.application.usecase.impl;

import com.monitorjbl.xlsx.StreamingReader;
import com.vvhung.application.usecase.AbstractReadExcelFile;
import com.vvhung.application.usecase.ImportUseCase;
import com.vvhung.application.usecase.TemplateUseCase;
import com.vvhung.application.utils.excel.StreamObjectUpload;
import com.vvhung.business.business.usecases.JobErrorUseCase;
import com.vvhung.business.business.usecases.JobLogUseCase;
import com.vvhung.business.domain.model.JobError;
import com.vvhung.business.domain.model.JobLog;
import com.vvhung.common.shared.constant.ImportError;
import com.vvhung.common.shared.constant.JobStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
@RequiredArgsConstructor
public class ReadExcelUseCase extends AbstractReadExcelFile implements ImportUseCase {
    private final JobLogUseCase jobLogUsecase;
    private final TemplateUseCase templateUseCase;
    private final StreamObjectUpload objectUpload;
    private final JobErrorUseCase jobErrorUseCase;

    @Override
    public JobLog importData(InputStream is) {
        JobLog jobLog = new JobLog();
        jobLog.setJobName("Import Data v1");
        jobLogUsecase.createJobLog(jobLog);

        InputStream cloneIS = cloneInputStream(is);
        if (cloneIS != null) {
            CompletableFuture.runAsync(() -> importData(jobLog, cloneIS));
        }
        return jobLog;
    }

    public void importData(JobLog jobLog, InputStream dataIS) {
        log.info("create errorWorkbook");
        SXSSFWorkbook errorWorkbook = new SXSSFWorkbook(100);
        boolean hasError = false;

        ZipSecureFile.setMinInflateRatio(0.0001);
        try (Workbook workbook = StreamingReader.builder().rowCacheSize(100)    // number of rows to keep in memory (defaults to 10)
                .bufferSize(4096)     // buffer size to use when reading InputStream to file (defaults to 1024)
                .open(dataIS)) {

            hasError = importTemplate(jobLog, getSheetByName(workbook, "name sheet"), errorWorkbook) || hasError;

            jobLog.setStatus(JobStatus.COMPLETED);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            jobLog.setStatus(JobStatus.FAILED);
            jobLog.setErrorMessage(ex.getMessage());
        } finally {
            if (hasError)
                saveErrorsToFile(errorWorkbook, jobLog);
            jobLogUsecase.saveJobLog(jobLog);
        }
    }

    private boolean importTemplate(JobLog jobLog, Sheet sheet, SXSSFWorkbook errorWorkbook) throws InstantiationException, IllegalAccessException {
        if (sheet == null) {
            return false;
        }
        log.info("Start import Template!!!, job: {}", jobLog.getRequestId());
        Sheet errorSheet = errorWorkbook.createSheet(sheet.getSheetName());
        boolean result = templateUseCase.importExcel(sheet, errorSheet, jobLog);
        log.info("Finish import Template!!!, job: {}", jobLog.getRequestId());
        return result;
    }

    public static InputStream cloneInputStream(InputStream input) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = input.read(buffer)) > -1) {
                byteArrayOutputStream.write(buffer, 0, len);
            }
            byteArrayOutputStream.flush();
            return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        } catch (Exception ex) {
            return null;
        }
    }

    private Sheet getSheetByName(Workbook workbook, String sheetName) {
        Sheet sheet;
        try {
            sheet = workbook.getSheet(sheetName);
        } catch (Exception ex) {
            sheet = null;
        }
        return sheet;
    }

    private void saveErrorsToFile(SXSSFWorkbook errorWorkbook, JobLog jobLog) {
        try {
            String fileName = String.format("%s.xlsx", jobLog.getRequestId());
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            errorWorkbook.write(bos);
            jobLog.setErrorFileUrl(objectUpload.streamObjectUpload(fileName, bos));

            bos.close();
            errorWorkbook.dispose();
        } catch (Exception e) {
            jobErrorUseCase.saveJobError(createJobError(ImportError.EI0008, jobLog));
        }
    }

    private JobError createJobError(ImportError importError, JobLog jobLog) {
        return JobError.builder()
                .requestId(jobLog.getRequestId())
                .errorCode(importError.name())
                .errorMessage(importError.getValue())
                .column("")
                .row(0)
                .sheetName(null).build();
    }
}
