package com.vvhung.application.usecase.impl;

import com.vvhung.application.usecase.TemplateUseCase;
import com.vvhung.application.usecase.dto.TemplateDto;
import com.vvhung.application.utils.excel.SheetData;
import com.vvhung.business.business.usecases.JobErrorUseCase;
import com.vvhung.business.domain.Template;
import com.vvhung.business.domain.model.JobLog;
import com.vvhung.common.shared.constant.ImportAction;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class TemplateUseCaseImpl extends BaseImportUseCaseImpl<TemplateDto, Template> implements TemplateUseCase {
    private SheetData sheetData;
    private final Validator validator;
    private final JobErrorUseCase jobErrorUseCase;

    @Override
    public boolean importExcel(Sheet sheet, Sheet errorSheet, JobLog jobLog) throws InstantiationException, IllegalAccessException {
        sheetData = new SheetData(sheet, HEADER_INDEX, BEGIN_DATA_INDEX);

        HashMap<String, String[]> actionConfigs = new HashMap<>() {{
            put("INSERT", new String[]{"", "", "Require", "", "Require", "Require", "", "Require", "Require", "Number", "", "", "", "", "", "Require", "Require", "", "", "", "", "RequireNumber", "RequireNumber", "Require", "Require", "Require", "RequireNumber", "RequireNumber", "Require", "RequireNumber", "Number", "Require", "Require", "Number", "Number", "Number", "Number", "Number", "Number", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "Number", "Number", "Date", "", "", "", "", "Require", "Require", "RequireNumber", "", ""});
            put("UPDATE", new String[]{"", "", "Require", "", "Require", "Require", "Require", "Require", "Require", "Number", "", "", "", "", "", "Require", "Require", "", "", "", "", "RequireNumber", "RequireNumber", "Require", "Require", "Require", "RequireNumber", "RequireNumber", "Require", "RequireNumber", "Number", "Require", "Require", "Number", "Number", "Number", "Number", "Number", "Number", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "Number", "Number", "Date", "", "", "", "", "Require", "Require", "RequireNumber", "", ""});
            put("DELETE", new String[]{"", "", "", "", "Require", "", "Require"});
        }};
        String[] configs = new String[]{"", "", "", "", "", "", ""};
        super.init(actionConfigs, new TemplateDto());
        return super.importExcel(sheet, errorSheet, jobLog);
    }

    @Override
    protected void processData(List<TemplateDto> listData) {
        String[] actions = sheetData.getColumn(ACTION_INDEX);

        for (int actionIndex = 0; actionIndex < actions.length; actionIndex++) {
            if (sheetData.getTotalError(actionIndex + BEGIN_DATA_INDEX - 1) > 0) continue;

            ImportAction action = StringUtils.hasText(actions[actionIndex]) ? ImportAction.valueOf(actions[actionIndex]) : ImportAction.INSERT;
            TemplateDto templateDto = listData.get(actionIndex);
            Template template = mapData(templateDto, action);

            processData(template, action);
        }
    }

    private Template mapData(TemplateDto templateDto, ImportAction action) {
        // map data from dto to domain model
        return new Template();
    }

    protected void processData(Template template, ImportAction action) {
        if (template != null) {
            try {
                switch (action) {
                    case INSERT:
                        //Todo
                    case UPDATE:
                        //Todo
                    case DELETE:
                        //Todo
                }
            } catch (Exception e) {

            }
        }
    }

    @Override
    protected JobErrorUseCase getJobErrorUseCase() {
        return jobErrorUseCase;
    }

    @Override
    protected Validator getValidator() {
        return validator;
    }
}
