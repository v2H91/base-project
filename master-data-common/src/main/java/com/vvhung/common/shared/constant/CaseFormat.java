package com.vvhung.common.shared.constant;

public enum CaseFormat {
    LOWER_CAMEL, UPPER_CAMEL, NONE;

    public static boolean isValid(String value) {
        CaseFormat.valueOf(value);
        return true;
    }
}
