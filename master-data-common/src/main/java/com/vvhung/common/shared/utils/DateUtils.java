package com.vvhung.common.shared.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

public final class DateUtils {
    private static final String YEAR_MONTH_FORMAT = "yyMM";
    private static final String DEFAULT_VN_DATE_FORMAT = "dd/MM/yyyy";
    private static final String DEFAULT_VN_TIME_FORMAT = "HH:mm:ss";

    private static final int DF_PART_COUNT = 3;
    private static final int DF_YEAR_INDEX = 2;
    private static final int DF_MONTH_INDEX = 1;
    private static final int DF_DAY_INDEX = 0;

    private DateUtils() {
    }

    public static boolean isFormat(String dateStr, String format) {
        if (StringUtils.isEmpty(dateStr) || StringUtils.isEmpty(format)) {
            throw new IllegalArgumentException("All params must not empty");
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            simpleDateFormat.setLenient(false);
            return simpleDateFormat.parse(dateStr) != null;
        } catch (ParseException ex) {
            return false;
        }
    }

    public static Date getDate(String dateStr, String format) {
        if (StringUtils.isEmpty(dateStr) || StringUtils.isEmpty(format)) {
            throw new IllegalArgumentException("All params must not empty");
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            simpleDateFormat.setLenient(false);
            return simpleDateFormat.parse(dateStr);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static Date getStartOfDay(Date fromDate) {
        if (Objects.isNull(fromDate))
            return null;

        return Date.from(LocalDate.ofInstant(fromDate.toInstant(), ZoneOffset.UTC)
                .atTime(0, 0, 0)
                .toInstant(ZoneOffset.UTC));
    }

    public static Date getEndOfDay(Date toDate) {
        if (Objects.isNull(toDate))
            return null;

        return Date.from(LocalDate.ofInstant(toDate.toInstant(), ZoneOffset.UTC)
                .atTime(23, 59, 59)
                .toInstant(ZoneOffset.UTC));
    }

    public static LocalDateTime to(Date date) {
        return Optional.ofNullable(date)
                .map(d -> LocalDateTime.ofInstant(d.toInstant(), ZoneOffset.UTC))
                .orElse(null);
    }

    public static String getCurrentYearMonth() {
        return new SimpleDateFormat(YEAR_MONTH_FORMAT).format(new Date());
    }

    /**
     * Transform date in format dd/MM/yyyy to yyyy-MM-dd
     *
     * @param dateInSlashFormat
     * @return formatted date, or empty if input is not valid
     */
    public static String dateFormatSlashToHyphen(String dateInSlashFormat) {
        if (Objects.isNull(dateInSlashFormat)) {
            return StringUtils.EMPTY;
        }
        String[] dobs = dateInSlashFormat.split("/");
        if (dobs.length == DF_PART_COUNT) {
            return dobs[DF_YEAR_INDEX] + "-"
                    + dobs[DF_MONTH_INDEX] + "-"
                    + dobs[DF_DAY_INDEX];
        }
        return StringUtils.EMPTY;
    }

    public static String formatVnDate(Instant time) {
        if (Objects.isNull(time)) {
            return "";
        }
        return DateTimeFormatter.ofPattern(DEFAULT_VN_DATE_FORMAT)
                .withZone(ZoneId.systemDefault())
                .format(time);
    }

    public static String formatVnTime(Instant time) {
        if (Objects.isNull(time)) {
            return "";
        }
        return DateTimeFormatter.ofPattern(DEFAULT_VN_TIME_FORMAT)
                .withZone(ZoneId.systemDefault())
                .format(time);
    }
}