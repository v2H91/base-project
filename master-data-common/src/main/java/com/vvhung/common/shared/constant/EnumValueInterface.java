package com.vvhung.common.shared.constant;

public interface EnumValueInterface {
    String getValue();

    int getCode();

    boolean checkExist(String value);
}
