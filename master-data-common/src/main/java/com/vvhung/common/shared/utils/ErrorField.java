package com.vvhung.common.shared.utils;

import lombok.Builder;
import lombok.Data;

import javax.validation.ConstraintViolation;

@Data
@Builder
public class ErrorField {

    private final String field;
    private final String message;

    public static ErrorField of(ConstraintViolation<?> violation) {
        return ErrorField.builder()
                .field(violation.getPropertyPath().toString())
                .message(violation.getMessage())
                .build();
    }

    public static ErrorField of(String field, String message) {
        return ErrorField.builder()
                .field(field)
                .message(message)
                .build();
    }

    public String toString() {
        return field + ": " + message;
    }
}
