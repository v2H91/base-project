package com.vvhung.common.shared.constant;

public enum ImportAction {
    INSERT, UPDATE, DELETE;

    public static boolean isValid(String value) {
        return ImportAction.valueOf(value) != null;
    }
}
