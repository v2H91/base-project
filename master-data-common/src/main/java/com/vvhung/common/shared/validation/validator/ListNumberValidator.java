package com.vvhung.common.shared.validation.validator;

import com.vvhung.common.shared.validation.annotation.ListNumberFormat;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ListNumberValidator implements ConstraintValidator<ListNumberFormat, Object> {

    private long min;

    private long max;

    private boolean checkRange;

    @Override
    public void initialize(ListNumberFormat numberFormat) {
        min = numberFormat.min();
        max = numberFormat.max();
        checkRange = numberFormat.checkRange();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext cxt) {
        List<String> numbers = new ArrayList<>();

        if (value instanceof String && !ObjectUtils.isEmpty(value)) {
            numbers = List.of(value.toString().split(";"));
        }
        if (value instanceof List) {
            numbers = (List<String>) value;
        }
        try {
            if (!checkRange) return true;
            for (String number : numbers) {
                BigDecimal dValue = new BigDecimal(number);
                if (dValue.compareTo(BigDecimal.valueOf(min)) < 0
                        || dValue.compareTo(BigDecimal.valueOf(max)) > 0) return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
