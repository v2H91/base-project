package com.vvhung.common.shared.utils;

import lombok.experimental.UtilityClass;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@UtilityClass
public class ValidationUtil {

    public <T> List<ErrorField> validate(T input) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(input);
        return violations.stream().map(ErrorField::of).collect(Collectors.toList());
    }

    public <T> List<ErrorField> validate(T input, Map<String, String> jsonFieldMap) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(input);
        return violations.stream().map(x -> {
            String field = x.getPropertyPath().toString();
            String jsonField = jsonFieldMap.get(field);
            if (jsonField != null) {
                field = jsonField;
            }
            return ErrorField.of(field, x.getMessage());
        }).collect(Collectors.toList()
        );
    }
}
