package com.vvhung.common.shared.validation.annotation;

import com.vvhung.common.shared.validation.validator.ListNumberValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {ListNumberValidator.class})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@ReportAsSingleViolation
public @interface ListNumberFormat {
    String message() default "must be positive numbers";

    long min() default 0;

    long max() default Long.MAX_VALUE;

    boolean checkRange() default true;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
