package com.vvhung.common.shared.constant;

public enum MonthEnum implements EnumValueInterface {
    JANUARY(1, "jan"),
    FEBRUARY(2, "feb"),
    MARCH(3, "mar"),
    APRIL(4, "apr"),
    MAY(5, "may"),
    JUNE(6, "jun"),
    JULY(7, "jul"),
    AUGUST(8, "aug"),
    SEPTEMBER(9, "sep"),
    OCTOBER(10, "oct"),
    NOVEMBER(11, "nov"),
    DECEMBER(12, "dec_dpm");
    private int code;
    private String value;

    MonthEnum(int code, String value) {
        this.value = value;
        this.code = code;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public boolean checkExist(String value) {
        MonthEnum[] monthEnums = MonthEnum.values();
        for (MonthEnum monthEnum : monthEnums) {
            if (monthEnum.getValue().equals(value)) return true;
        }
        return false;
    }
}
