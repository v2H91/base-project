package com.vvhung.common.shared.validation.validator;

import com.vvhung.common.shared.validation.annotation.GreaterThan;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class GreaterThanValidator implements ConstraintValidator<GreaterThan, Object> {
    private String field;
    private String fieldCompare;
    private boolean hasEqual;

    @Override
    public void initialize(GreaterThan annotation) {
        this.field = annotation.field();
        this.fieldCompare = annotation.fieldCompare();
        this.hasEqual = annotation.hasEqual();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        try {
            BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);
            Object fieldValue = wrapper.getPropertyValue(field);
            Object fieldValueCompare = wrapper.getPropertyValue(fieldCompare);
            List<String> fieldValues = new ArrayList<>();
            List<String> fieldValuesCompare = new ArrayList<>();
            boolean isGreaterThan = true;

            if (fieldValue instanceof String && !ObjectUtils.isEmpty(fieldValue)) {
                fieldValues = List.of(fieldValue.toString().split(";"));
                fieldValuesCompare = List.of(fieldValueCompare.toString().split(";"));
            }
            if (fieldValue instanceof List) {
                fieldValues = (List<String>) fieldValue;
                fieldValuesCompare = (List<String>) fieldValueCompare;
            }
            if (fieldValues.size() != fieldValuesCompare.size()) return false;
            for (int valueIndex = 0; valueIndex < fieldValues.size(); valueIndex++) {
                if (!hasEqual)
                    isGreaterThan &= Double.parseDouble(fieldValues.get(valueIndex)) > Double.parseDouble(fieldValuesCompare.get(valueIndex));
                else
                    isGreaterThan &= Double.parseDouble(fieldValues.get(valueIndex)) >= Double.parseDouble(fieldValuesCompare.get(valueIndex));
            }

            return isGreaterThan;
        } catch (Exception e) {
            return true;
        }
    }
}