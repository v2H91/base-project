package com.vvhung.common.shared.validation.validator;

import com.vvhung.common.shared.validation.annotation.PhoneFormat;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidator implements ConstraintValidator<PhoneFormat, String> {

    private final Pattern phonePattern = Pattern.compile("^(((\\+|)84)|0)(1|3|5|7|8|9)([0-9]{8})\\b$");

    @Override

    public void initialize(PhoneFormat numberFormat) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext cxt) {
        if (StringUtils.hasText(value)) {
            Matcher matcher = phonePattern.matcher(value);
            return matcher.matches();
        }
        return true;
    }
}
