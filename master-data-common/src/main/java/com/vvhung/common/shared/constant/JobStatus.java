package com.vvhung.common.shared.constant;

public enum JobStatus {
    PROCESSING, FAILED, COMPLETED;

    public static boolean isValid(String value) {
        return JobStatus.valueOf(value) != null;
    }
}
