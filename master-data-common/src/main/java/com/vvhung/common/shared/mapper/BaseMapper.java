package com.vvhung.common.shared.mapper;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Component
@Slf4j
public class BaseMapper {
    public static final String FORMAT_PATTERN = "yyyy-MM-dd";

    public static final String DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm";

    public String fromLocalDate(LocalDate date) {
        if (date == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT_PATTERN);
        return date.format(formatter);
    }

    public String fromDate(Date value) {
        if (value != null) {
            return value.toLocalDate().format(DateTimeFormatter.ofPattern(FORMAT_PATTERN));
        }
        return null;
    }

    public Date toDate(String value) {
        if (StringUtils.hasText(value)) {
            try {
                return Date.valueOf(LocalDate.parse(value, DateTimeFormatter.ofPattern(FORMAT_PATTERN)));
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public String fromInstant(Instant value) {
        if (value != null) {
            LocalDateTime localDateTime = LocalDateTime.ofInstant(value, ZoneOffset.UTC);

            return DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN).format(localDateTime);
        }
        return null;
    }

    public Instant toInstant(String value) {
        if (StringUtils.hasText(value)) {
            try {
                LocalDateTime localDateTime = LocalDateTime.parse(value, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN));
                return localDateTime.toInstant(ZoneOffset.UTC);
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public Instant toInstant(Long value) {
        if (Objects.nonNull(value)) {
            return Instant.ofEpochMilli(value);
        }
        return null;
    }

    public String trimStr(String value) {
        if (value != null) {
            try {
                return value.trim();
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public List<String> toList(String value) {
        if (value != null) {
            try {
                LinkedList<String> list = new LinkedList<>(Arrays.asList(value.trim().split("\\s*;\\s*")));
                return list;
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public List<BigDecimal> toListBigDecimal(String value) {
        if (value != null) {
            try {
                List<String> listStr = Arrays.asList(value.split(";"));
                List<BigDecimal> list = new ArrayList<>();
                for (String s : listStr) {
                    list.add(new BigDecimal(s));
                }
                return list;
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public String toString(List<String> list) {
        if (list != null && !Objects.equals(list.toString(), "[]")) {
            try {
                return String.join(";", list);
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public BigDecimal toBigDecimal(String value) {
        if (value != null) {
            try {
                return NumberUtils.parseNumber(value, BigDecimal.class);
            } catch (Exception ex) {
                return null;
            }
        }
        return null;
    }

    public Boolean toBoolean(String value) {
        if (value != null && value.equalsIgnoreCase("yes"))
            return true;
        if (value != null && value.equalsIgnoreCase("no"))
            return false;

        return null;
    }
}
