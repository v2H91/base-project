package com.vvhung.common.shared.validation.validator;

import com.vvhung.common.shared.validation.annotation.DateFormat;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class DateValidator implements ConstraintValidator<DateFormat, String> {
    private String min;
    private String max;
    private String pattern;

    @Override
    public void initialize(DateFormat dateFormat) {
        pattern = dateFormat.pattern();
        this.min = dateFormat.min();
        this.max = dateFormat.max();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext cxt) {
        if (StringUtils.hasText(value)) {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date date;

            try {
                date = sdf.parse(value);
                if (!value.equals(sdf.format(date))) {
                    return false;
                }
                if ("".equals(min) && "".equals(max)) return true;

                return LocalDate.parse(min).isBefore(LocalDate.parse(value)) &&
                        LocalDate.parse(max).isAfter(LocalDate.parse(value));
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
}

