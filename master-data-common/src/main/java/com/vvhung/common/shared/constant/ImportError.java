package com.vvhung.common.shared.constant;

public enum ImportError {

    // REQUIRED ERROR
    EI0000("unknown error"),
    EI0001("is required"),
    EI0002("is incorrect date format or is not exist date"),
    EI0003("is not a number"),
    EI0004("is not a boolean"),
    EI0005("is duplicated"),
    EI0006("Action is invalid"),
    EI0007("do not send approval request"),
    EI0008("do not upload file error"),
    EI0009("one of fields is required"),
    EI0010("value is incorrect"),
    EI0011("value must existed at least 1 value"),
    EI0012("is error cell");

    private String value;

    ImportError(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static boolean isValid(String value) {
        return ImportError.valueOf(value) != null;
    }

    public static ImportError fromString(String text) {
        for (ImportError importError : ImportError.values()) {
            if (importError.value.equalsIgnoreCase(text)) {
                return importError;
            }
        }

        return EI0000;
    }
}
