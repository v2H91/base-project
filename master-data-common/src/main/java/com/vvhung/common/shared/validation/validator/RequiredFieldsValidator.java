package com.vvhung.common.shared.validation.validator;

import com.vvhung.common.shared.constant.CaseFormat;
import com.vvhung.common.shared.validation.annotation.RequiredFields;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RequiredFieldsValidator implements ConstraintValidator<RequiredFields, Object> {
    private String field;
    private String message;
    private String messageSingle;
    private CaseFormat caseFormat;

    @Override
    public void initialize(RequiredFields annotation) {
        this.field = annotation.field();
        this.message = annotation.message();
        this.messageSingle = annotation.messageSingle();
        this.caseFormat = annotation.caseFormat();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);
        List<String> requiredFields = (List<String>) wrapper.getPropertyValue(field);
        List<String> emptyFields = emptyFields(requiredFields, wrapper);

        if (emptyFields.size() > 0) {
            String message = emptyFields.size() == 1 ? this.messageSingle : this.message;

            setValidationErrorMessage(context, String.join(", ", emptyFields) + " " + message);
            return false;
        }
        return true;
    }

    private String standardFieldName(String fieldName) {
        switch (caseFormat) {
            case LOWER_CAMEL:
                return com.google.common.base.CaseFormat.LOWER_UNDERSCORE.to(com.google.common.base.CaseFormat.LOWER_CAMEL, fieldName);
            case UPPER_CAMEL:
                return com.google.common.base.CaseFormat.LOWER_UNDERSCORE.to(com.google.common.base.CaseFormat.UPPER_CAMEL, fieldName);
            default:
                return fieldName;
        }
    }

    private List<String> emptyFields(List<String> fields, BeanWrapper wrapper) {
        List<String> empties = new ArrayList<>();

        if (fields == null)
            return empties;
        for (String field : fields) {
            try {
                String fieldName = standardFieldName(field);
                Object fieldValue = wrapper.getPropertyValue(fieldName);

                if (fieldValue == null || StringUtils.isEmpty(fieldValue.toString())) empties.add(field);
            } catch (NotReadablePropertyException ex) {
                log.error(ex.getMessage());
            }
        }

        return empties;
    }

    private void setValidationErrorMessage(ConstraintValidatorContext context, String template) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(template).addConstraintViolation();
    }
}