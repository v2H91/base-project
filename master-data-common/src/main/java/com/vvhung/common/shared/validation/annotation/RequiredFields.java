package com.vvhung.common.shared.validation.annotation;

import com.vvhung.common.shared.constant.CaseFormat;
import com.vvhung.common.shared.validation.validator.RequiredFieldsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {RequiredFieldsValidator.class})
public @interface RequiredFields {
    String message() default "are required";
    String messageSingle() default "is required";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String field() default "requiredFields";
    CaseFormat caseFormat() default CaseFormat.LOWER_CAMEL;
}