package com.vvhung.common.shared.validation.annotation;

import com.vvhung.common.shared.validation.validator.GreaterThanValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {GreaterThanValidator.class})
@Repeatable(GreaterThan.List.class)
public @interface GreaterThan {
    String message() default "must be greater than";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String field();

    String fieldCompare();

    boolean hasEqual() default false;

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        GreaterThan[] value();
    }
}