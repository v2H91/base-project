package com.vvhung.business.domain.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemEvent extends AbstractEvent{
    private Long id;
    private String name;
    private String category;
}
