package com.vvhung.business.domain.model;


import com.vvhung.common.shared.constant.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobLog implements Serializable {
    private JobStatus status = JobStatus.PROCESSING;
    private String jobName;
    private String requestId = UUID.randomUUID().toString();
    private String errorMessage;
    private List<JobError> jobErrors;
    private String errorFileUrl;
    private String pageIndex;
}
