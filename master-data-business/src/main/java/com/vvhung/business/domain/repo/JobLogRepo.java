package com.vvhung.business.domain.repo;


import com.vvhung.business.domain.model.JobLog;

public interface JobLogRepo {

    JobLog createJobLog(JobLog jobLog);

    JobLog saveJobLog(JobLog jobLog);

    JobLog getByRequestId(String requestId);

    void deleteByRequestId(String requestId);
}
