package com.vvhung.business.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class JobError implements Serializable {
    private Long id;
    private int row;
    private String errorMessage;
    private String errorMessageDetail;
    private String errorCode;
    private String requestId;
    private String sheetName;
    private String column;
}
