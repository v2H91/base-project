package com.vvhung.business.domain.repo.impl;

import com.vvhung.business.domain.model.JobError;
import com.vvhung.business.domain.repo.JobErrorRepo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JobErrorRepoImpl implements JobErrorRepo {
    @Override
    public JobError saveJobError(JobError jobError) {
        return null;
    }

    @Override
    public List<JobError> findAllByRequestId(String requestId, int pageIndex, int size) {
        return null;
    }
}
