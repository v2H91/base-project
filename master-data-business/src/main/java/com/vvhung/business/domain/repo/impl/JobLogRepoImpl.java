package com.vvhung.business.domain.repo.impl;

import com.vvhung.business.domain.model.JobLog;
import com.vvhung.business.domain.repo.JobLogRepo;
import org.springframework.stereotype.Repository;

@Repository
public class JobLogRepoImpl implements JobLogRepo {
    @Override
    public JobLog createJobLog(JobLog jobLog) {
        return null;
    }

    @Override
    public JobLog saveJobLog(JobLog jobLog) {
        return null;
    }

    @Override
    public JobLog getByRequestId(String requestId) {
        return null;
    }

    @Override
    public void deleteByRequestId(String requestId) {

    }
}
