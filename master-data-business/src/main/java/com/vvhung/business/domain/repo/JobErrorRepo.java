package com.vvhung.business.domain.repo;

import com.vvhung.business.domain.model.JobError;

import java.util.List;

public interface JobErrorRepo {

    JobError saveJobError(JobError jobError);

    List<JobError> findAllByRequestId(String requestId, int pageIndex, int size);
}
