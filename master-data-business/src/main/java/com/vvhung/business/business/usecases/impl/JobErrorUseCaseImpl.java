package com.vvhung.business.business.usecases.impl;

import com.vvhung.business.business.usecases.JobErrorUseCase;
import com.vvhung.business.domain.model.JobError;
import com.vvhung.business.domain.repo.JobErrorRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class JobErrorUseCaseImpl implements JobErrorUseCase {

    private final JobErrorRepo jobErrorRepo;

    @Override
    public JobError saveJobError(JobError jobError) {
        return jobErrorRepo.saveJobError(jobError);
    }
}
