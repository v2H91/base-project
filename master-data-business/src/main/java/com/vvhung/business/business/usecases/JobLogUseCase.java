package com.vvhung.business.business.usecases;


import com.vvhung.business.domain.model.JobLog;

public interface JobLogUseCase {
    JobLog createJobLog(JobLog jobLog);

    JobLog saveJobLog(JobLog jobLog);

    JobLog getLogByRequestId(String requestId);

    JobLog getLogByRequestId(String requestId, int pageIndex);

    void deleteByRequestId(String requestId);
}
