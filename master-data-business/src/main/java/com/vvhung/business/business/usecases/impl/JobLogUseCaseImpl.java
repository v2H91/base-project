package com.vvhung.business.business.usecases.impl;

import com.vvhung.business.business.usecases.JobLogUseCase;
import com.vvhung.business.domain.model.JobError;
import com.vvhung.business.domain.model.JobLog;
import com.vvhung.business.domain.repo.JobErrorRepo;
import com.vvhung.business.domain.repo.JobLogRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class JobLogUseCaseImpl implements JobLogUseCase {

    private final JobLogRepo jobLogRepo;
    private final JobErrorRepo jobErrorRepo;

    @Override
    public JobLog createJobLog(JobLog jobLog) {
        return jobLogRepo.createJobLog(jobLog);
    }

    @Override
    public JobLog saveJobLog(JobLog jobLog) {
        return jobLogRepo.saveJobLog(jobLog);
    }

    @Override
    public JobLog getLogByRequestId(String requestId) {
        return getLogByRequestId(requestId, 0);
    }

    @Override
    public JobLog getLogByRequestId(String requestId, int pageIndex) {
        JobLog jobLog = jobLogRepo.getByRequestId(requestId);
        List<JobError> jobErrors = jobErrorRepo.findAllByRequestId(requestId, pageIndex, 100);

        jobLog.setJobErrors(jobErrors);
        jobLog.setPageIndex(String.valueOf(pageIndex));

        return jobLog;
    }

    @Override
    public void deleteByRequestId(String requestId) {
        jobLogRepo.deleteByRequestId(requestId);
    }
}
