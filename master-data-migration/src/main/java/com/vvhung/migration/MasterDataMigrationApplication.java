package com.vvhung.migration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasterDataMigrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(MasterDataMigrationApplication.class, args);
    }

}
