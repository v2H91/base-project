package com.vvhung.worker.functions;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;

public class ValidateAnnotationTest {
    private Validator validator;

    @BeforeEach
    public void setup() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void t01_test_validate_annotations() {
        String regex = "(.*) must be greater than or equal to ([^ ]*)";
        String message = "number_width_max must be greater than or equal to number_width_min";
        ValidateAnnotation validateAnnotation = new ValidateAnnotation();

        validateAnnotation.setNumberWidthMins(List.of("3", "4", "5", "10"));
        validateAnnotation.setNumberWidthMaxes(List.of("4", "5", "6", "11"));
        validateAnnotation.setBooleanField("yse");

        Set<ConstraintViolation<ValidateAnnotation>> errors = validator.validate(validateAnnotation);

        System.out.println(message.matches(regex));
        System.out.println(message.replaceAll(regex, "$1,$2"));
        System.out.println(errors);
    }
}
