package com.vvhung.worker.functions;

import com.vvhung.common.shared.validation.annotation.GreaterThan;
import com.vvhung.common.shared.validation.annotation.ListNumberFormat;
import com.vvhung.common.shared.validation.annotation.NumberFormat;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@GreaterThan(field = "numberWidthMaxes", fieldCompare = "numberWidthMins", message = "number_width_max must be greater than or equal to number_width_min", hasEqual = true)
public class ValidateAnnotation {
    @ListNumberFormat(max = 100, message = "number_width_min must be in 0 to 100 range")
    private String numberWidthMin;
    @ListNumberFormat(max = 100, message = "number_width_max must be in 0 to 100 range")
    private String numberWidthMax;
    @ListNumberFormat(max = 100, message = "number_width_min must be in 0 to 100 range")
    private List<@NumberFormat String> numberWidthMins;
    @ListNumberFormat(max = 100, message = "number_width_max must be in 0 to 100 range")
    private List<@NumberFormat String> numberWidthMaxes;
    @Pattern(regexp = "(?i)(Yes)|(?i)(No)", message = "booleanField must be Yes or No")
    private String booleanField;
}
