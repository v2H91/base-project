package com.vvhung.worker.controller;

import com.vvhung.business.business.usecases.JobLogUseCase;
import com.vvhung.worker.dto.JobLogResponse;
import com.vvhung.worker.dto.JobRequest;
import com.vvhung.worker.jobs.JobRegistry;
import com.vvhung.worker.mapper.JobLogResponseMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping(JobsController.JOBS)
@RequiredArgsConstructor
public class JobsController {

    public static final String JOBS = "/v1/jobs";

    private final JobRegistry jobRegistry;
    private final JobLogUseCase jobLogUsecase;
    private final JobLogResponseMapper responseMapper;

    @PostMapping
    public JobLogResponse createJob(@RequestBody @Valid JobRequest job) throws InterruptedException {
        return responseMapper.from(jobRegistry.run(job.getName(), job.getData()));
    }

    @GetMapping("/{requestId}")
    public JobLogResponse getLogResult(@PathVariable String requestId) {
        return responseMapper.from(jobLogUsecase.getLogByRequestId(requestId, 0));
    }

    @GetMapping("/{requestId}/{pageIndex}")
    public JobLogResponse getLogResultPage(@PathVariable String requestId, @PathVariable int pageIndex) {
        return responseMapper.from(jobLogUsecase.getLogByRequestId(requestId, pageIndex));
    }

    @DeleteMapping("/{requestId}")
    public void deleteJobLog(@PathVariable String requestId) {
        jobLogUsecase.deleteByRequestId(requestId);
    }
}
