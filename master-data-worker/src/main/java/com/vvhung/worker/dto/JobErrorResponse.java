package com.vvhung.worker.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@AllArgsConstructor
@Data
public class JobErrorResponse implements Serializable {
    private String errorMessage;
    private String errorMessageDetail;
    private String errorCode;
    private String sheetName;
    private int row;
    private String column;
}
