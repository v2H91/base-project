package com.vvhung.worker.dto;

import com.vvhung.common.shared.constant.JobStatus;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@Data
public class JobLogResponse implements Serializable {
    private JobStatus status;
    private String jobName;
    private String requestId;
    private String errorMessage;
    private List<JobErrorResponse> jobErrors;
    private String errorFileUrl;
    private String pageIndex;
}
