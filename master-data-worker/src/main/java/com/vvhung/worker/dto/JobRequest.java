package com.vvhung.worker.dto;


import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;


@Data
public class JobRequest {

    @NotBlank
    private String name;
    @Valid
    private JobDataDto data;
}
