package com.vvhung.worker.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Map;

@Data
public class JobDataDto {

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    String dataUpdateDate;

    Map<String, Object> params;
}
