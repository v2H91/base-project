package com.vvhung.worker.jobs;

import com.vvhung.business.business.usecases.JobLogUseCase;
import com.vvhung.business.domain.model.JobLog;
import com.vvhung.common.shared.constant.JobStatus;
import com.vvhung.worker.dto.JobDataDto;

import java.util.concurrent.CompletableFuture;

public interface Job {
    JobLog run(JobDataDto data) throws InterruptedException;

    default JobLog runJobFunction(JobLogUseCase jobLogUsecase, Class clazz, Runnable runnable) {

        JobLog jobLog = new JobLog();
        jobLog.setJobName(clazz.getSimpleName());
        jobLogUsecase.createJobLog(jobLog);

        CompletableFuture.runAsync(() -> {
            try {
                runnable.run();
                jobLog.setStatus(JobStatus.COMPLETED);
            } catch (Exception ex) {
                jobLog.setStatus(JobStatus.FAILED);
                jobLog.setErrorMessage(ex.getMessage());
            } finally {
                jobLogUsecase.saveJobLog(jobLog);
            }
        });

        return jobLog;
    }
}
