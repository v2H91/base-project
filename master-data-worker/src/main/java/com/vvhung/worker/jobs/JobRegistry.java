package com.vvhung.worker.jobs;

import com.vvhung.business.domain.model.JobLog;
import com.vvhung.worker.dto.JobDataDto;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class JobRegistry {

    private final Map<String, Job> jobs;

    public JobRegistry(Map<String, Job> jobs) {
        this.jobs = jobs;
    }

    public JobLog run(String name, JobDataDto data) throws InterruptedException {
        final Job job = jobs.get(name);
        if (job == null) {
            throw new IllegalArgumentException(String.format("job name [%s] is not exist, existing jobs [%s]", name, jobs.keySet()));
        }
        return job.run(data);
    }
}
