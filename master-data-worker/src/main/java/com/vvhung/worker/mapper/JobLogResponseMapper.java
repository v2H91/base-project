package com.vvhung.worker.mapper;

import com.vvhung.business.domain.model.JobLog;
import com.vvhung.common.shared.mapper.BaseMapper;
import com.vvhung.common.shared.mapper.IObjectMapper;
import com.vvhung.worker.dto.JobLogResponse;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {
        BaseMapper.class
})
public interface JobLogResponseMapper extends IObjectMapper<JobLog, JobLogResponse> {
}
