package com.vvhung.worker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.vvhung.*"})
public class MasterDataWorkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MasterDataWorkerApplication.class, args);
    }

}
