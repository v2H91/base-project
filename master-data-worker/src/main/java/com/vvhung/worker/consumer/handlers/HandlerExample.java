package com.vvhung.worker.consumer.handlers;


import com.vvhung.business.domain.event.AbstractEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class HandlerExample {
    @KafkaListener(topics = "test", groupId = "group-id")
    public void listen(ConsumerRecord<String, AbstractEvent> record) {
        System.out.println("Received Messasge in group - group-id: " + record.value());
    }
}
